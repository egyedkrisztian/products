## Leírás

A felület termékek adminisztrálására szolgál, több nyelven.

<b>Kezelt adatok:</b>

- termék név
- termék leírás
- cimke felhő
- termék kép
- publikál kezdete
- publikálás vége
- termék ár

<b>Funkciók:</b>

- adatlap létrehozás
- adatlap módosítás
- adatlap inaktiválás
- adatlap törlés

## Telepítés

1. Állományok felmásolása a szerverre
2. .ENV állományban adatok megadása (url, adatbázis)
3. Adatbázis migrálás / telepítés
4. Nyelvek felvitele a "languages" táblába<br />
Minta: name -> "Magyar" short_name -> "hu"

## Adatbázis
Adatbázis stuktúra a ".db_schema" mappa alatt található.

## Képernyőképek
A felületről készített mintaképek a ".screenshot" mappa alatt találahtó.
