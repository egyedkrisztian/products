<?php

namespace App\Http\Controllers;

use App\Product;
use App\Language;
use App\LanguageData;
use App\Tag;
use App\TagData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductsController extends Controller
{
    public function list()
    {
        // List all product

        $products = Product::where('status', 1)->orderBy('name', 'asc')->get();

        return view('index', ['products' => $products]);
    }

    public function create()
    {
        // Show a view to create a new product

        $languages = Language::orderBy('name', 'asc')->get();
        $tags = Tag::orderBy('name', 'asc')->get();


        return view('create', ['languages' => $languages, 'tags' => $tags]);
    }

    public function save(Request $request)
    {
        // Save new product

        $message = "";
        $image = "";
        $tag_id = array();
        $languages = Language::orderBy('name', 'asc')->get();

        request()->validate([
            'name' => 'required',
        ]);

        foreach ($languages as $language) {
            if (request('tags-'.$language->short_name)) {
                $tags = explode(",", request('tags-' . $language->short_name));

                foreach ($tags as $tag) {
                    $tag_item = Tag::where('language_id', $language->id)->where('name', $tag)->first();

                    if ($tag_item) {
                        array_push($tag_id, $tag_item->id);
                    }
                    else {
                        $item = Tag::create([
                            'language_id' => $language->id,
                            'name' => $tag,
                            'date' => NOW(),
                        ]);
                        array_push($tag_id, $item->id);
                    }
                }
            }

            if (request('tag_id-'.$language->short_name)) {
                foreach (request('tag_id-' . $language->short_name) as $tag) {
                    array_push($tag_id, $tag);
                }
            }
        }

        if ($request->image) {
            $image = $request->image->getClientOriginalName();
            $request->image->move(public_path("products"), $image);
        }

        $item = Product::create([
            'name' => request('name'),
            'image' => $image,
            'publishing_first_day' => request('publishing_first_day'),
            'publishing_last_day' => request('publishing_last_day'),
            'price' => request('price'),
            'created_at' => NOW(),
            'updated_at' => NOW(),
        ]);

        foreach ($languages AS $language) {
            LanguageData::create([
                'product_id' => $item->id,
                'language_id' => $language->id,
                'text' => request('text-' . $language->short_name),
                'created_at' => NOW(),
                'updated_at' => NOW(),
            ]);
        }

        foreach ($tag_id AS $id) {
            TagData::create([
                'product_id' => $item->id,
                'tag_id' => $id,
                'date' => NOW(),
            ]);
        }

        if ($item) {
            $message = "Sikeres létrehozás.";
        }
        else {
            $message = "Sikertelen létrehozás, próbáld meg később.";
        }

        return Redirect::to('/')->with('message', $message);
    }

    public function edit($id)
    {
        // Show a selected product

        $languages = Language::orderBy('name', 'asc')->get();
        $product = Product::where('id', $id)->first();

        $languagesData = array();
        foreach ($languages AS $language) {
            $languageData = LanguageData::where('product_id', $id)->where('language_id', $language->id)->first();
            if ($languageData) {
                $languagesData[$language->id]['id'] = $languageData->id;
                $languagesData[$language->id]['text'] = $languageData->text;
            }
            else {
                $languagesData[$language->id]['id'] = NULL;
                $languagesData[$language->id]['text'] = NULL;
            }
        }

        $tags = Tag::orderBy('name', 'asc')->get();
        $tagsData = TagData::where('product_id', $id)->get();

        return view('edit', ['product_id' => $id, 'languages' => $languages, 'product' => $product, 'languageData' => $languagesData, 'tags' => $tags, 'tagsData' => $tagsData]);
    }

    public function update(Request $request, $product_id)
    {
        // Update in exist product

        $message = "";
        $image = "";
        $tag_id = array();
        $product = Product::find($product_id);
        $languages = Language::orderBy('name', 'asc')->get();

        request()->validate([
            'name' => 'required',
        ]);

        foreach ($languages as $language) {
            if (request('tags-'.$language->short_name)) {
                $tags = explode(",", request('tags-' . $language->short_name));

                foreach ($tags as $tag) {
                    $tag_item = Tag::where('language_id', $language->id)->where('name', $tag)->first();

                    if ($tag_item) {
                        array_push($tag_id, $tag_item->id);
                    }
                    else
                    {
                        $item = Tag::create([
                            'language_id' => $language->id,
                            'name' => $tag,
                            'date' => NOW(),
                        ]);
                        array_push($tag_id, $item->id);
                    }
                }
            }

            if (request('tag_id-'.$language->short_name)) {
                foreach (request('tag_id-' . $language->short_name) as $tag) {
                    array_push($tag_id, $tag);
                }
            }
        }

        if ($request->image) {
            $image = $request->image->getClientOriginalName();
            $request->image->move(public_path("products"), $image);
        }
        else {
            $image = request('image_name');
        }

        $result = $product->update([
            'name' => request('name'),
            'image' => $image,
            'publishing_first_day' => request('publishing_first_day'),
            'publishing_last_day' => request('publishing_last_day'),
            'price' => request('price'),
            'updated_at' => NOW(),
        ]);

        foreach ($languages AS $language) {
            $language_data_item = LanguageData::where('language_id', $language->id)->where('product_id', $product_id)->first();

            if ($language_data_item) {
                $language_data_item->update([
                    'text' => request('text-' . $language->short_name),
                ]);
            }
            else {
                LanguageData::create([
                    'product_id' => $product_id,
                    'language_id' => $language->id,
                    'text' => request('text-' . $language->short_name),
                    'created_at' => NOW(),
                    'updated_at' => NOW(),
                ]);
            }
        }

        TagData::where('product_id', $product_id)->delete();
        foreach ($tag_id AS $id) {
            TagData::create([
                'product_id' => $product_id,
                'tag_id' => $id,
                'date' => NOW(),
            ]);
        }

        if ($result) {
            $message = "Sikeres módosítás.";
        }
        else {
            $message = "Sikertelen módosítás, próbáld meg később.";
        }

        return redirect()->back()->with('message', $message);
    }

    public function hide($product_id)
    {
        // Hide the selected product

        $message = "";
        $product = Product::find($product_id);

        $result = $product->update([
            'status' => "0",
            'updated_at' => NOW(),
        ]);

        if ($result) {
            $message = "Sikeres elrejtés.";
        }

        return redirect()->back()->with('message', $message);
    }

    public function delete($product_id)
    {
        // Delete the selected product

        $message = "";
        $product = Product::find($product_id);

        if ($product->image) {
            File::delete(public_path("products") . "/" . $product->image);
        }
        LanguageData::where('product_id', $product_id)->delete();
        TagData::where('product_id', $product_id)->delete();
        $result = Product::where('id', $product_id)->delete();

        if ($result) {
            $message = "Sikeres törlés.";
        }
        else {
            $message = "Sikertelen törlés, próbáld meg később.";
        }

        return redirect()->back()->with('message', $message);
    }

    public function deleteImage($product_id)
    {
        // Delete the selected product image

        $message = "";
        $product = Product::find($product_id);

        File::delete(public_path("products") . "/" . $product->image);

        $result = $product->update([
            'image' => "",
            'updated_at' => NOW(),
        ]);

        if ($result) {
            $message = "Sikeres törlés.";
        }
        else {
            $message = "Sikertelen törlés, próbáld meg később.";
        }

        return redirect()->back()->with('message', $message);
    }
}
