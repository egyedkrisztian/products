<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagsController extends Controller
{
    public static function check($value, $object) {
        if (is_object($object)) {
            foreach($object as $key => $item) {
                if ($value==$item->tag_id) return TRUE;
            }
        }
        return false;
    }
}
