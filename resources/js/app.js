require('./bootstrap');

import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';
$('.datepicker').datepicker({
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    changeMonth: true,
    changeYear: true,
});
