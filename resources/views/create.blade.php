@extends('layout')

@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">

                <h3 class="heading text-center">Termékek</h3>
                <h5 class="heading text-center">Új termék</h5>
                <br />

                <div class="text-right margin-bottom-20">
                    <a class="btn btn-outline-secondary" href="/products/public/">Vissza</a>
                </div>

                <form method="POST" action="/products/public/" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <input type="hidden" name="">

                    <div class="form-group">
                        <label for="name"><b>Termék név</b></label>

                        <div>
                            <input class="form-control {{ $errors->has('name') ? 'border-danger' : '' }}" type="text" name="name" id="name" value="{{ old('name') }}">

                            @error('name')
                                <p class="help text-danger">A név megadása kötelező!</p>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            @foreach($languages AS $id => $language)
                                <li class="nav-item">
                                    <a class="nav-link {{ $id == 0 ? "active" : "" }}" href="#{{ $language->short_name }}">{{ $language->name }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content border-bottom border-left border-right">

                            @foreach($languages AS $id => $language)
                                <div class="tab-pane {{ $id == 0 ? "active" : "" }}" id="{{ $language->short_name }}" role="tabpanel" aria-labelledby="{{ $language->short_name }}-tab">
                                    <div class="form-group">
                                        <label for="text-{{ $language->short_name }}"><b>Leírás</b></label>
                                        <textarea class="form-control editor" name="text-{{ $language->short_name }}" id="text-{{ $language->short_name }}"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="labels-{{ $language->short_name }}"><b>Címkék</b></label>
                                        <input class="form-control {{ $id == 0 ? "active" : "" }}" type="text" name="tags-{{ $language->short_name }}" id="tags-{{ $language->short_name }}" value="">
                                        <p class="small">Az új címkéket, vesszővel elválasztva kell rögzíteni.</p>

                                        @foreach($tags AS $tag)
                                            @if ($tag->language_id == $language->id)
                                                <div class="form-check form-check-inline tag-button">
                                                    <input class="form-check-input" type="checkbox" id="tag_id-{{ $language->short_name }}[{{ $tag->id }}]" name="tag_id-{{ $language->short_name }}[{{ $tag->id }}]" value="{{ $tag->id }}">
                                                    <label class="form-check-label" for="tag_id-{{$language->short_name}}[{{ $tag->id }}]">{{ $tag->name }}</label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <script>
                            tinymce.init({
                                selector:'.editor',
                                height: 300,
                                menubar: false,
                            });

                            $('.nav-tabs a').on('click', function (e) {
                                e.preventDefault()
                                $(this).tab('show')
                            })
                        </script>

                    </div>

                    <div class="form-group">
                        <label for="text"><b>Termék kép</b></label>
                        <input class="form-control-file" type="file" name="image" id="image" value="">
                    </div>

                    <div class="form-group">
                        <label for="text"><b>Publikálás</b></label>
                        <div class="form-inline">
                            <input class="form-control datepicker" type="text" name="publishing_first_day" id="publishing_first_day" value=""> <span class="p-1">-</span> <input class="form-control datepicker" type="text" name="publishing_last_day" id="publishing_last_day" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="text"><b>Termék ár</b></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2">€</span>
                            </div>
                            <input class="form-control" type="text" name="price" id="price" value="">
                        </div>
                    </div>

                    <div class="form-group padding-top-20 text-center">
                        <button type="submit" class="btn btn-success">Rögzít</button>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
