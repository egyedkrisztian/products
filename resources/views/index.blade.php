@extends('layout')

@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">

                <h3 class="heading text-center">Termékek</h3>
                <br />

                <div class="text-right margin-bottom-20">
                    <a class="btn btn-outline-success" href="create">Új termék</a>
                </div>

                @if (session()->has('message'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if (count($products) > 0)
                    <table id="table-products" class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Név</th>
                                <th>Publikálás kezdete</th>
                                <th>Publikálás vége</th>
                                <th class="text-right">Ár</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach ($products as $product)
                                <tr>
                                    <td>
                                        @if ($product->image)
                                            <img src="/products/public/products/{{ $product->image }}" class="thumbnail">
                                        @endif
                                    </td>
                                    <td class="align-middle">{{ $product->name }}</td>
                                    <td class="align-middle">{{ $product->publishing_first_day }}</td>
                                    <td class="align-middle">{{ $product->publishing_last_day }}</td>
                                    <td class="align-middle text-right"> @money($product->price) </td>
                                    <td class="align-middle text-right">
                                        <a href="product/{{ $product->id }}/hide" title="Elrejt" onclick="return confirm('Biztos elrejted a terméket?')"><i class="fa fa-ban" aria-hidden="true"></i></a>
                                        <a href="product/{{ $product->id }}/delete" title="Töröl" onclick="return confirm('Biztos törlöd a terméket?')"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        <a href="product/{{ $product->id }}" title="Kiválaszt"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-center font-italic border-top padding-top-20">Nincs rögzített termék.</p>
                @endif

            </div>
        </div>
    </div>

@endsection
