<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BioTechUSA - Products</title>

    <link rel="stylesheet" href="/products/public/css/app.css">
    <link rel="stylesheet" href="/products/public/css/style.css">
    <script src="/products/public/js/app.js" charset="utf-8"></script>
    <script src="/products/public/js/main.js" charset="utf-8"></script>

    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
</head>
<body>

    @yield('content')

</body>
</html>
