<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ProductsController@list');
Route::get('/create', 'ProductsController@create');
Route::post('/', 'ProductsController@save');
Route::get('/product/{id}', 'ProductsController@edit');
Route::put('/product/{id}', 'ProductsController@update');
Route::get('/product/{id}/hide', 'ProductsController@hide');
Route::get('/product/{id}/delete', 'ProductsController@delete');
Route::get('/product/{id}/delete-image', 'ProductsController@deleteImage');
